<?php
namespace Utm;

/**
 * Class UtmManager
 * Класс для работы с utm метками
 *
 * Пример использования:
 *
 * <code>
 *
 * <?php
 *      $utmManager = new UtmManager($_GET);
 *      $campaign = $utmManager->getValueByName("word");
 *      echo "Клиент пришел по ключевому слову $campaign";
 * ?>
 *
 * </code>
 */
class UtmManager
{

    /**
     * @var array - Массив utm меток. В качестве ключей выступают названия utm меток без приставки "utm_"
     */
    private $utmContainer = [];


    /**
     * @param $utmList - массив, содержащий utm метки. Обычно $_GET или $_POST.
     */
    public function __construct($utmList)
    {
        foreach($utmList as $key => $value) {
            if (preg_match("/utm_(.*)/", $key, $matches)) {
                $this->utmContainer[$matches[1]] = $value;
            }
        }
    }


    /**
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @return string - Значение utm метки
     */
    public function getValueByName($name)
    {
        if (isset($this->utmContainer[$name])) {
            return $this->utmContainer[$name];
        }
        return "";
    }


    /**
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @return bool
     */
    public function exists($name)
    {
        if (isset($this->utmContainer[$name])) {
            return true;
        }
        return false;
    }


    /**
     * Метод проверяет равна ли utm метка $name значению $value
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @param $value - проверяемой значение метки
     * @return bool
     */
    public function is($name, $value)
    {
        return $this->getValueByName($name) == $value;
    }


    /**
     * @return string - Строка в виде строки GET параметров, например "utm_campaign=google&utm_keyword=example&"
     * !!! Параметры utm в этой строке идут с приставкой "utm_"
     */
    public function getAsUrlParamsString()
    {
        $utmContainer = $this->utmContainer;
        $res = "";
        foreach ($utmContainer as $name => $value) {
            $res .= $this->getOriginalUtmName($name) . "=" . $value . "&";
        }
        return substr($res, 0, -1);
    }


    /**
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @return string - название utm метки с приставкой "utm_"
     */
    protected function getOriginalUtmName($name)
    {
        return "utm_" . $name;
    }


    /**
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @return string - строка html кода. Скрытый тег input с аттрибутом name равным названию utm метки с приставкой "utm_"
     * и аттрибутом value равным значению utm метки
     */
    public function getAsHiddenInput($name)
    {
        return "<input type=\"hidden\" name=\"". $this->getOriginalUtmName($name). "\" value=\"" . $this->getValueByName($name) . "\">";
    }


    /**
     * Метод не изменяет GET и POST параметры, а изменяет только $this->utmContainer
     * @param $name - название utm метки, без приставки "utm_", то есть если метка параметр utm_campaign, то просто "campaign"
     * @param $value - новое значение utm метки
     */
    public function set($name, $value)
    {
        $this->utmContainer[$name] = $value;
    }


    /**
     * @return string - Строка html кода, содержащая скрытые теги input с аттрибутом name равным названию utm метки с приставкой "utm_"
     * и аттрибутом value равным значению utm метки ДЛЯ ВСЕХ utm меток.
     */
    public function getAllUtmAsHiddenInput()
    {
        $res = "";
        foreach ($this->utmContainer as $name => $value) {
            $res .= $this->getAsHiddenInput($name) . "\n";
        }
        return $res;
    }


    /**
     * @param $filePath - путь к файлу шаблона .php . Внутри шаблона доступны переменные с названиями как у ключей массива $params
     * См. пример файла mail_template.php и index.php
     *
     * @param $params - массив переменных, доступных внутри шаблона filePath
     *
     * @return string - Результат шаблона
     */
    public function getUtmForMail($filePath, $params)
    {
        ob_start();

        // Костыль! Сделано чтобы не было коллизий при передаче в $params ключей key и value
        foreach ($params as $key_syghesrkyuvgewriuye2324t5g45t => $value_cn679w5twir7f94g678t345) {
            $$key_syghesrkyuvgewriuye2324t5g45t = $value_cn679w5twir7f94g678t345;
        }

        include $filePath;
        $mailContent = ob_get_clean();
        return $mailContent;
    }


    /**
     * @return array - Массив utm меток, в качестве ключей у которого выступают названия utm меток с приставкой "utm_"
     */
    public function getAllUtmAsOriginal()
    {
        $originalList = [];
        foreach ($this->utmContainer as $key => $value) {
            $originalList[$this->getOriginalUtmName($key)] = $value;
        }
        return $originalList;
    }
}