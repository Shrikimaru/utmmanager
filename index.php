<?php
include_once "vendor/autoload.php";

use Utm\UtmManager;

$utmManager = new UtmManager($_GET);

echo $utmManager->getUtmForMail("mail_template.php", [
    "privet" => "hi",
    "poka" => "bye"
]);
?>